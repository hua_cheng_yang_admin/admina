import axios from 'axios';
var CancelToken = axios.CancelToken;
var cancel;
const service = axios.create({
	// process.env.NODE_ENV === 'development' 来判断是否开发环境
	// easy-mock服务挂了，暂时不使用了
	baseURL: 'http://81.71.136.172:8888',
	timeout: 5000,
	headers: {
		token: 'asd'
	},
	cancelToken: new CancelToken((c)=> {
		// executor 函数接收一个 cancel 函数作为参数
		cancel = c;
	})
});
// 可通过axios.defaults.headers.dddd = 123;设置全局默认

service.interceptors.request.use(
	config => {
		return config;
	},
	error => {
		console.log(error);
		return Promise.reject();
	}
);

service.interceptors.response.use(
	response => {
		if (response.status === 200) {
			return response.data;
		} else {
			Promise.reject();
		}
	},
	error => {
		console.log(error);
		return Promise.reject();
	}
);

export default service;
