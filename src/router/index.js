import Vue from 'vue';
import Router from 'vue-router';
Vue.use(Router);

export default new Router({
	// mode:'history',
    routes: [
        {
            path: '/',
            redirect: '/login'
        },
        {
            path: '/',
            component: () => import(/* webpackChunkName: "home" */ '../components/common/Home.vue'),
            meta: { title: '自述文件' },
            children: [
				
                {
                    path: '/index',
					props: true,
                    component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/index.vue'),
                    meta: { title: '系统首页' }
                },
				{
				    path: '/index1',
				    component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/index1.vue'),
				    meta: { title: '系统首页1' }
				},
				{
				    path: '/index2',
				    component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/index2.vue'),
				    meta: { title: '系统首页2' }
				},
				{
				    path: '/center',
				    component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/center.vue'),
				    meta: { title: '个人中心' }
				},
                {
                    path: '/404',
                    component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/404.vue'),
                    meta: { title: '页面走丢了' }
                },
                {
                    path: '/403',
                    component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/403.vue'),
                    meta: { title: '系统首页' }
                }
            ]
        },
        {
            path: '/login',
            component: () => import(/* webpackChunkName: "login" */ '../components/page/Login.vue'),
            meta: { title: '登录' }
        },
        {
            path: '*',
            redirect: '/404'
        }
    ]
});
