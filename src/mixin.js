import {
	mapState,
	mapMutations,
	mapGetters ,
	mapActions  
} from 'vuex'
import store from "./store.js"
// 尝试将用户在根目录中的store/index.js的vuex的state变量，全部加载到全局变量中
let $uStoreKey = [];
var getss = [];
for(let i in store.getters){
	getss.push(i);
}
try {
	$uStoreKey = store.state ? Object.keys(store.state) : [];
} catch (e) {

}
var MyPlugin = {};
MyPlugin.install = function(Vue, options) {
	// 1. 添加全局方法或属性
	Vue.myGlobalMethod = function() {
		// 逻辑...
	}

	// 2. 添加全局资源
	Vue.directive('my-directive', {
		bind(el, binding, vnode, oldVnode) {
			// 逻辑...
		}
	})

	// 3. 注入组件选项
	Vue.mixin({
		data() {
			return {
				created: [],
				activated: [],
				mounted: [],
				updated: [],
				destroyed:[]
			}
		},
		runfunc(vue) {
			// 调用周期函数 vue.runfunc(数据类型string/[] c:created m:mounted a:activated u:updated d:destroyed,func/运行代码块)
			// vue.runfunc(['a','c'],function(){
			// 	vue.boxcheck(()=>{
			// 		vue.height =  $('.content').height() - $('.table').position().top + 30
			// 		vue.$refs.singleTable.setCurrentRow(vue.tableData[2]);//默认选择列
			// 	})
			// })
			// vue.runfunc('m',function(){
			// 	vue.boxcheck(()=>{
			// 		console.log(vue.height)
			// 	})
				
			// })
		},
		beforeCreate() {
			// 将vuex方法挂在到$u中
			// 使用方法为：如果要修改vuex的state中的user.name变量为"史诗" => this.$u.vuex('user.name', '史诗')
			// 如果要修改vuex的state的version变量为1.0.1 => this.$u.vuex('version', '1.0.1')
			this.vuex = (name, value, time) => {
				this.$store.commit('$uStore', {
					name,
					value,
					time
				})
			}
		},
		created() {
			this.$options.runfunc ? this.$options.runfunc(this) : null;
			this.created.forEach((key)=>{
				key();
			})
		},
		activated(){
			this.activated.forEach((key)=>{
				key();
			})
		},
		mounted(){
			this.mounted.forEach((key)=>{
				key();
			})
		},
		filters:{
			dateymdhms(fmts){
				if (!fmts) return ;
				Date.prototype.format = function (fmt) {
					var o = {
					"M+": this.getMonth() + 1, //月份
					"d+": this.getDate(), //日
					"h+": this.getHours(), //小时
					"m+": this.getMinutes(), //分
					"s+": this.getSeconds(), //秒
					"q+": Math.floor((this.getMonth() + 3) / 3), //季度
					"S": this.getMilliseconds() //毫秒
					};
					if (/(y+)/.test(fmt)) {
					fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
					}
					for (var k in o) {
					if (new RegExp("(" + k + ")").test(fmt)) {
					fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
					}
					}
					return fmt;
				}
				return fmts.format('yyyy/MM/dd hh:mm:ss')
			}
		},
		directives:{
			
		},
		computed: {
			// 将vuex的state中的所有变量，解构到全局混入的mixin中
			...mapState($uStoreKey),
			...mapGetters(getss)
		},
		methods: {
			...mapMutations(store._mutations),
			...mapActions(store._actions),
			language(num){
			},
			dateformat(fmts,type){
				if (!fmts) return ;
				Date.prototype.format = function (fmt) {
					var o = {
					"M+": this.getMonth() + 1, //月份
					"d+": this.getDate(), //日
					"h+": this.getHours(), //小时
					"m+": this.getMinutes(), //分
					"s+": this.getSeconds(), //秒
					"q+": Math.floor((this.getMonth() + 3) / 3), //季度
					"S": this.getMilliseconds() //毫秒
					};
					if (/(y+)/.test(fmt)) {
					fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
					}
					for (var k in o) {
					if (new RegExp("(" + k + ")").test(fmt)) {
					fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
					}
					}
					return fmt;
				}
				return !type?fmts.format('yyyy/MM/dd hh:mm:ss'):fmts.format(type)
			},
			runfunc(type,func){
				if(typeof (type) == 'string'){
					if(type == 'c' || !type){
						this.created.push(func)
					}else if(type == 'm'){
						this.mounted.push(func)
					}else if(type == 'a'){
						this.activated.push(func)
					}else if(type == 'u'){
						this.updated.push(func)
					}else{
						this.destroyed.push(func)
					}
				}else{
					type.forEach((key)=>{
						if(key == 'c'){
							this.created.push(func)
						}else if(key == 'm'){
							this.mounted.push(func)
						}else if(key == 'a'){
							this.activated.push(func)
						}else if(type == 'u'){
							this.updated.push(func)
						}else{
							this.destroyed.push(func)
						}
					})
				}
				
			},
			boxcheck(func) {
				// console.log(func)
				if (this.$nextTick) {
					this.$nextTick(() => {
						func();
						window.onresize = () => {
							func();
						}
					})
				} else {
					func();
					window.onresize = () => {
						func();
					}
				}
			}
		}
	})
}
export default MyPlugin;
