import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

let lifeData = {};

try{
	// 尝试获取本地是否存在lifeData变量，第一次启动APP时是不存在的
	lifeData = $cookies.get('lifeData')?$cookies.get('lifeData'):{};
	
}catch(e){
	
}

// 需要永久存储，且下次APP启动需要取出的，在state中的变量名
let saveStateKeys = ['vuex_user'];

// 保存变量到本地存储中
const saveLifeData = function(key, value,time){
	if(!time){
		time = '0';
	}
	// 判断变量名是否在需要存储的数组中
	// if(saveStateKeys.indexOf(key) != -1) {
		// 获取本地存储的lifeData对象，将变量添加到对象中
		let tmp = $cookies.get('lifeData')?$cookies.get('lifeData'):{};
		// 第一次打开APP，不存在lifeData变量，故放一个{}空对象
		tmp[key] = value;
		// 执行这一步后，所有需要存储的变量，都挂载在本地的lifeData对象中
		$cookies.set('lifeData', JSON.stringify(tmp),time);
	// }
}
const store = new Vuex.Store({
	state: {
		// 如果上面从本地获取的lifeData对象下有对应的属性，就赋值给state中对应的变量
		// 加上vuex_前缀，是防止变量名冲突，也让人一目了然
		vuex_user: lifeData.vuex_user ? lifeData.vuex_user : null,
		menu:lifeData.menu ? lifeData.menu : [],
		tagsList:lifeData.tagsList?lifeData.tagsList:[],
		user:lifeData.user ? lifeData.user : false,
		sotitles:lifeData.sotitles?lifeData.sotitles:{},
		url:'http://127.0.0.1:10004',
		bgtype:lifeData.bgtype?lifeData.bgtype:'rgb(106,106,106)',//背景主题
		texttype:lifeData.texttype?lifeData.texttype:'#fff',//字体主题
		activetexttype:lifeData.activetexttype?lifeData.activetexttype:'#20a0ff',//选中字体
		collapses:lifeData.collapses
	},
	mutations: {
		$uStore(state, payload) {
			// 判断是否多层级调用，state中为对象存在的情况，诸如user.info.score = 1
			let nameArr = payload.name.split('.');
			let saveKey = '';
			let len = nameArr.length;
			if(len >= 2) {
				let obj = state[nameArr[0]];
				for(let i = 1; i < len - 1; i ++) {
					obj = obj[nameArr[i]];
				}
				obj[nameArr[len - 1]] = payload.value;
				saveKey = nameArr[0];
			} else {
				// 单层级变量，在state就是一个普通变量的情况
				state[payload.name] = payload.value;
				saveKey = payload.name;
			}
			// 保存变量到本地，见顶部函数定义
			saveLifeData(saveKey, state[saveKey],payload.time)
		},
		a(){
			console.log(123)
		}
	},
	getters:{
		ass(state){
			return state;
		}
	},
	actions:{
		  increment ({ commit }) {
		    commit('a')
		  }
	}
})

export default store
