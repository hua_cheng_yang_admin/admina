


var fenxiaoapi = {
    httpGet(url, Succ,Field) {
			if (window.XMLHttpRequest) {
				var AjaxXML = new XMLHttpRequest();
			}
			else {
				var AjaxXML = new ActiveXObject("Microsoft.XMLHTTP");
			}
			AjaxXML.open("GET", url, true);
			AjaxXML.send();
			AjaxXML.onreadystatechange = function () {
				if (AjaxXML.readyState == 4) {
					if (AjaxXML.status == 200) {
						Succ(AjaxXML.responseText);//成功的时候调用这个方法
					}
					else {
						if (fnfiled) {
							Field(AjaxXML.status);
						}
					}
				}
			};
	},
	AutoAgentNoPass(vuethis,id,suc1,fila1){
		$.ajax({
		    url:vuethis.url + '/autoagent_delagent',
		    data:{ 
		        'token' : vuethis.vuex_user.token,
		        'id' : id
		    },
		    success:(data)=>{
		        suc1(data);
		    },
		    error(){
		        fila1(data);
		    }
		});
	},
	AutoAgentPass(vuethis,id,datajson,suc1,fila1){
		$.ajax({
		    url:vuethis.url + '/autoagent_checkagent',
		    data:{ 
		        'token' : vuethis.vuex_user.token,
		        'datajson' : datajson,
				'id' : id
		    },
		    success:(data)=>{
		        suc1(data);
		    },
		    error(){
		        fila1(data);
		    }
		});
	},
	GetOneAgents(vuethis,strselect,pageindex,pagesize,suc1,fila1){
		$.ajax({
		    url:vuethis.url + '/agent_list',
		    data:{ 
		        'token' : vuethis.vuex_user.token,
		        'strselect' : strselect,
				'limit' : pagesize,
				'page' : pageindex
		    },
		    success:(data)=>{
		        suc1(data);
		    },
		    error(){
		        fila1(data);
		    }
		});
	}
};

export default fenxiaoapi;